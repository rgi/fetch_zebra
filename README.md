Fetch Zebra
===========


## Quickstart

1. npm install
2. npm install -g
3. npm link
4. fetch_zebra --help

Start hacking index.js for the main cli app, and package.json for dependencies.

### TODO
* asynchronous fetching
* get canonical set of search terms for RareShare
* implement filtering by cui/UMLS OR
* dynamically expand disease list using terms not in search list
* set unique constraints on db (low priority)