#!/usr/bin/env node --harmony

'use strict'
var program = require('commander')
var nedb = require('nedb')
var parse = require('csv-parse/lib/sync')
var fs = require('fs')
var _ = require('lodash')
var http = require('es6-request')

const baseURL = 'http://www.findzebra.com/api/call/json/query'
const num_docs = 100

console.log('Finding Zebra... How about them stripes?')

function increaseVerbosity(v, total) {
  return total + 1;
}

program
  .option('-d, --database <db_name>', 'The NeDB database name')
  .option('-s, --search <filename>', 'A csv file with the first column holding search terms.')
  .option('-v, --verbose', 'Show results as they are fetched.')
  .parse(process.argv)

console.log('using database: %s', program.database)
console.log('using termfile: %s', program.search)

var db = new nedb({filename: program.database, autoload: true})

var terms = _.map(
  parse( fs.readFileSync( program.search, {encoding: 'utf8'}) ),
  _.first )

terms.forEach( (term) => {
  http.get(baseURL)
    .query({
      "score":"score desc",
      "fl":"display_title,content,cui,source_url",
      "rows": num_docs,
      "q": term
    }).then( (body) => {
      let res = JSON.parse(body)
      let found = res.response.numFound
      console.log("\nFound %d results for %s, showing first one.", found, term)
      let docs = res.response.docs
      console.log(docs[0])
      db.insert(docs)
      db.count({}, (err, count) => {
        console.log('collection expanded to %d docs', count)
      })
    })
})

